#!/bin/bash

if [ "$EUID" -ne 0 ] ; then
  >&2 echo "Please run as root, or use sudo"
  exit 1
fi

say() {
    >&2 echo "[+] $*"
}

# make sure we start from a clean state
say "Removing existing scripts..."
rm -f \
  /usr/local/bin/deploy.py \
  /usr/local/bin/initcap.py \
  /usr/local/bin/callback.py

type -t pip3 > /dev/null || {
    say "Install dependencies..."
    apt-get --quiet --quiet update
    [[ $( dpkg -l | grep -E -c '^i[^i]' ) -ne 0 ]] && dpkg --configure -a
    apt-get --quiet --quiet --yes install python3-pip
}
python3 -m pid 2>&1 | grep -F -q 'No module named pid' \
    && pip3 --disable-pip-version-check install pid

say "Downloading and installing scripts..."
until wget --quiet -O /usr/local/bin/deploy.py \
    https://gitlab.com/bibliosansfrontieres/olip/apps/cedj/-/raw/main/assets/deploy.py ; do
        sleep 10
    done
chmod +x /usr/local/bin/deploy.py

# run it
# we set this as a cronjob - the Deploy tarball may not be available locally yet so it has to try again and again
say "Checking for cronjob..."
croncmd="python3 /usr/local/bin/deploy.py > /tmp/deploy.py.log 2> /tmp/deploy.py.log"
if ! grep --no-messages --quiet "$croncmd" /var/spool/cron/crontabs/root ; then
  say "Installing cronjob..."
  echo -e "PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin\n*/10 * * * * $croncmd" > /tmp/deploy.cron \
  && crontab /tmp/deploy.cron && rm /tmp/deploy.cron
else
  say "The Cronjob already exists."
fi

say "Removing lock files..."
rm -f \
  /opt/olip-pre-install \
  /opt/olip-post-install \
  /opt/olip-deploy \
  /opt/rebooted

# Stop ongoing processes if any
say "Removing PID files..."
for i in deploy.pid olip_database_upgrade.pid ; do
  pidfile="/var/run/$i"
  [ ! -r "${pidfile}" ] && continue
  say "> found $pidfile"
  pid="$(cat $pidfile)"
  pgrep "$pid" && {
    say ">> found running pid ${pid}, killing..."
    kill -9 "$pid"
  }
  rm -f "$pidfile"
done

say "Done! Logs are about to be shown. Hit Ctrl+C to exit!"
say "Current time/date is $( date )"
echo
tail --lines=0 -F /var/log/ansible-pull.log /var/log/apt/history.log
