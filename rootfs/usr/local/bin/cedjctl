#!/bin/bash

set -eu

say() {
    echo "$( date '+%F %T') $( basename "$0" ): $*"
}
edebug() {
    [ -z "$DEBUG" ] && return
    echo "$( date '+%F %T') $( basename "$0" ): ##### DEBUG: $*"
}
DEBUG="${DEBUG:-}"

REMOTESERVER="${REMOTESERVER:-filer.bsf-intranet.org}"
REMOTEPATH="${REMOTEPATH:-cedj}"

UPSTREAM_DB_FILE='/data/justice.sql'

DOCUMENTROOT='/data/htdocs'
STATE_FILE='/data/latest'

DB_HOST="${DB_HOST:-cedj.app.mariadb}"
DB_NAME="${DB_NAME:-justice}"
DB_USER="${DB_USER:-cedjuser}"
DB_PASS="${DB_PASS:-cedjpass}"

usage(){
    cat <<EOF
Usage: $( basename "$0" ) [subcommand]

Main command:
    check_install       Ensure everything is set, does what is missing if needed

Available subcommands:
    do_update           Performs an update
                            (download_files + download_dbdump + db_import)
    db_init             Database initialization
                            (wait_for_mysql + db_create_db + download_dbdump + db_import)

Atomic subcommands:
    db_create_db        Creates the MySQL database and its user
    db_import           Imports a MySQL dump file into the database - and deletes it afterward
    download_files      Downloads files from upstream server
    download_dbdump     Downloads the database dump from upstream server
    update_statefile    Downloads the statefile from upstream server

Internal (private) subcommands:
    db_query            Run a MySQL query
    is_update_needed    Check whether an update is available
    wait_for_mysql      Blocks until the MySQL server is available
EOF
    exit
}

wait_for_internet() {
    edebug 'wait_for_internet()'
    tries=1
    # shellcheck disable=SC2046
    until wget --spider --user-agent=$( basename "$0") --quiet --no-check-certificate "https://${REMOTESERVER}/${REMOTEPATH}/latest" >/dev/null 2>&1 ; do
        say "Waiting for internet connectivity... (attempt ${tries}/10)"
        tries=$(( tries + 1 ))
        (( tries > 10 )) \
            && say 'No internet. Sleeping until the next run.' \
            && exit 0
        sleep 10
    done
}
wait_for_mysql() {
    edebug 'wait_for_mysql()'
    while [ ! -f /data/mariadb/mariadb_pwd ] ; do
        say 'MySQL server not ready...'
        sleep 2
    done
}
db_query() {
    MARIADB_PWD="$( cat /data/mariadb/mariadb_pwd )"
    mysql -uroot -p"${MARIADB_PWD}" -h "$DB_HOST" "$@"
}
db_drop_db() {
    edebug 'db_drop_db()'
    db_query -e "DROP DATABASE IF EXISTS $DB_NAME ;"
}
db_create_db() {
    edebug 'db_create_db()'
    say 'Create database...'
    db_query -e "CREATE DATABASE IF NOT EXISTS $DB_NAME DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
    say 'Create user...'
    db_query -e "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON $DB_NAME.* TO '$DB_USER'@'%' IDENTIFIED BY '$DB_PASS';"
}
db_reset_db() {
    db_drop_db
    db_create_db
}
download_dbdump() {
    edebug 'download_dbdump()'
    wait_for_internet
    say 'Download DB dump...'
    rsync -a --info=name "rsync://${REMOTESERVER}/${REMOTEPATH}/justice.sql" "$UPSTREAM_DB_FILE"
    chmod a+r "$UPSTREAM_DB_FILE"
}
db_import() {
    edebug 'db_import()'
    say 'Reset DB...'
    db_reset_db
    say 'Import DB dump...'
    db_query "$DB_NAME" < "$UPSTREAM_DB_FILE"
    say 'Delete DB dump...'
    rm -f "$UPSTREAM_DB_FILE"
}
download_files() {
    edebug 'download_files()'
    wait_for_internet
    say 'Download files...'
    rsync -a --info=name "rsync://${REMOTESERVER}/${REMOTEPATH}/htdocs/" "${DOCUMENTROOT}/"
    chmod a+rX -R "${DOCUMENTROOT}/"
    chown www-data:www-data -R "${DOCUMENTROOT}/"
}
update_statefile() {
    edebug 'update_statefile()'
    say 'Update the state file...'
    rsync -a --info=name "rsync://${REMOTESERVER}/${REMOTEPATH}/latest" "$STATE_FILE"
}

db_init() {
    say 'Database initialisation...'
    wait_for_mysql
    [ -f $UPSTREAM_DB_FILE ] || download_dbdump
    db_import
}
is_update_needed() {
    [ ! -f "$STATE_FILE" ] \
        && edebug 'is_update_needed(): no state file found.' \
        && return 1
    wait_for_internet
    # shellcheck disable=SC2046
    upstream_version="$( wget --user-agent=$( basename "$0") --quiet --no-check-certificate "https://${REMOTESERVER}/${REMOTEPATH}/latest" -O- )"
    edebug "Upstream version: $upstream_version"
    local_version="$( cat "$STATE_FILE" )"
    edebug "Local version: $local_version"
    (( upstream_version > local_version ))
}
do_update() {
    edebug 'do_update()'
    download_files
    download_dbdump
    db_import
    update_statefile
}
check_install() {
    if [ -f "$STATE_FILE" ] ; then
        edebug 'check_install(): State file found. Ckeching for updates...'
        is_update_needed \
            && edebug 'check_install(): update available.' \
            && do_update
        return 0 # avoid RC=1 when no update is needed
    else
        edebug 'check_install(): State file not found.'
        say 'No state file found.'
        db_init
        do_update
    fi
}



[[ "$#" -eq 0 || "$1" == "-h" || "$1" == "--help" || "$1" == "help" ]] \
    && usage

# main
$1
