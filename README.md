# cedj

Une application OLIP qui fournit les Bulletins Officiels du Burundi.

## Déploiement

Cette application ne figure PAS dans le catalogue général,
pour éviter de polluer le catalogue des déploiements non concernés.

Le déploiement d'un Ideascube lié à ce projet fait donc l'objet d'une procédure particulière.

En lieu et place de la commande `nokea` habituelle, il faudra utiliser ceci :

```shell
wget https://gitlab.com/bibliosansfrontieres/olip/apps/cedj/-/raw/main/assets/nokea.sh --quiet -O- | bash -
```

## Mises à jour des données

cf [USB_UPLOAD.md](USB_UPLOAD.md)

### Dans OLIP

Chaque heure, l'application vérifie la présence d'une nouvelle mise à jour à installer.

Le cas échéant, elle télécharge les fichiers et la base de données et les installe.

## TODO

OLIP App:

- ? Contents tweaks
  - replace it by something like « version offline du 6 brumaire »
  - bonus: RedirectMatch the connection link
    - ? ... to some page explaining the app's offline nature
- landing page until the App initialization is done

BDI server side:

- scripts:
  - extract files from the USB key (may stay manual,
    as long as we're not sure about the FS structure)

`bubble` side:

- document the service
- IaaS-ify the service
