#!/bin/bash

set -eu

say() {
    echo "$( date '+%F %T') $( basename "$0" ): $*"
}
edebug() {
    [ -z "$DEBUG" ] && return
    echo "$( date '+%F %T') $( basename "$0" ): ##### DEBUG: $*"
}
DEBUG="${DEBUG:-}"

export DEBUG

# is it initialized?
say 'Check install...'
cedjctl check_install

# supervisord
exec "$@"
