#!/bin/bash


set -eu

say() {
    echo "$( date '+%F %T') $( basename "$0" ): $*"
}
edebug() {
    [ -z "$DEBUG" ] && return
    echo "$( date '+%F %T') $( basename "$0" ): ##### DEBUG: $*"
}
DEBUG="${DEBUG:-}"
export DEBUG

GITCHECKOUTDIR="$(dirname "$(readlink -f "$0")")"

SRC="${SRC:-/media/hdd/cedj/usb}"
DEST_SERVER="${DEST_SERVER:-bubble.bsf-intranet.org}"
DEST_PATH="${DEST_PATH:-/home/www/filer.bsf-intranet.org/cedj}"
DEST="${DEST:-${DEST_SERVER}:${DEST_PATH}}"
PHPDBFILEURL='https://gitlab.com/bibliosansfrontieres/olip/apps/cedj/-/raw/main/assets/ouverture_bdd.php?ref_type=heads'

PHPDBFILE='assets/ouverture_bdd.php'
DBFILE="${SRC}/justice.sql"

die() {
    echo >&2 "Error: $*"
    exit 1
}

# shellcheck disable=SC2317
if [[ ! $EUID = 0 ]]; then
    die "You must be 'root' or run this script using sudo: sudo $( basename "$0" )"
    exit 3
fi

[ -r "$DBFILE" ]                                    || die "File not found or not readable: $DBFILE" 
[ -d "${SRC}/htdocs" ]                              || die "Directory not found: ${SRC}/htdocs" 
[ -w "${SRC}/htdocs/include/ouverture_bdd.php" ]    || die "File not writable: ${SRC}/htdocs/include/ouverture_bdd.php" 
[ -w "${SRC}/htdocs/entete_page.php" ]              || die "File not writable: ${SRC}/htdocs/entete_page.php" 

cd "$GITCHECKOUTDIR"                                || die "Can't cd to git checkout dir: $GITCHECKOUTDIR"

[ ! -r "$PHPDBFILE" ] && {
    say "PHP DB file not found (${PHPDBFILE}), download it from GitLab..."
    wget --quiet "$PHPDBFILEURL" -O "$PHPDBFILE" \
        || die "Unable to download main $PHPDBFILEURL"
}

say "Copy PHP DB file..."
cp --archive "$PHPDBFILE" "${SRC}/htdocs/include/ouverture_bdd.php"

say "Patch links in header logos..."
sed -i -e 's@http://localhost@http://cedj.ideascube.io@' "${SRC}/htdocs/entete_page.php"

say "Remove the user connection link..."
sed -i -e "s@<a href='login/login.php' target='_blank'> Espace Administration</a>@(version offline)@" \
    "${SRC}/htdocs/interpretation_actes.php"        \
    "${SRC}/htdocs/legislation_burundi.php"         \
    "${SRC}/htdocs/mesures_appli.php"               \
    "${SRC}/htdocs/resultat_rech_arret2.php"        \
    "${SRC}/htdocs/resultat_rech_arret21.php"       \
    "${SRC}/htdocs/resultat_rech_arret_test.php"    \
    "${SRC}/htdocs/resultat_rech_traite.php"        \
    "${SRC}/htdocs/traite.php"

say "Enforce ownership and filemodes..."
chown www-data:www-data -R "$SRC"
chmod a+rX,go-w -R "$SRC"

say "Remove users from 'utilisateurs' table..."
sed -i -e '/^INSERT INTO .utilisateurs/,/^$/d' "$DBFILE"

say "Clean up leftovers..."
# Parenthesis explained @ https://www.shellcheck.net/wiki/SC2146
find "${SRC}/htdocs/" \
    \( \
           -name '*so' \
        -o -name '*.dll' \
        -o -name '*.exe' \
        -o -name '*.bat' \
        -o -name '*.sql' \
        -o -name '*.sql.zip' \
        -o -name '*.sql *' \
        -o -name 'consultation.zip' \
        -o -name 'dossier consultation.zip' \
    \) \
    -ls \
    -delete

say "Tag state file..."
date "+%Y%m%d" > "${SRC}/latest"
chown www-data:www-data "${SRC}/latest"
chmod a+r "${SRC}/latest"

say "Upload..."
rsync -av "${SRC}/" "${DEST}/"

say "Generate a files listing..."
# shellcheck disable=SC2029
ssh "${DEST_SERVER}" "cd ${DEST_PATH}/ && tree -H '.' -T 'Liste des fichiers' -I 'fichiers.html' --noreport --charset utf-8 > fichiers.html"

say "Done."
