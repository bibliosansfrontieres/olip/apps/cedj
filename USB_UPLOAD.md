# Mise à jour depuis une clé USB

## Synopsis

Chaque ~mois, le Ministère de la Justice du Burundi fournit à BSF
une copie des données (fihciers et base de données).

La clé USB est insérée dans un serveur au sein des locaux de BSF Grands Lacs ;
son conteu est copié sur le serveur.

Le dossier étant destiné à être publié sur internet, il est alors nettoyé :

- on supprime les fichiers inutiles (`xampp-setup.exe`, ...)
- on supprime les comptes utilisateurs existants
- on modifie les identifiants de connexion à la base de données MySQL

Le dossier est alors publié sur internet, sur un serveur de BSF.

Son contenu est servi à l'adresse `filer.bsf-intranet.org/cedj/`
([`rsyncd`](rsync://filer.bsf-intranet.org/cedj/), [`http`](https://filer.bsf-intranet.org/cedj/))

Un fichier `/latest` contient la date de cette mise à jour.

## Copie des fichiers depuis la clé USB

Il existe plusieurs points de montage, prévus pour monter plusieurs partitions.
Dans le contexte du CEDJ, c'est une clé USB "classique" qui est utilisée
(comprendre, avec une seule partition formatée FAT32).

On insère la clé dans un port du serveur CDN,
et on la monte sur le point de montage approprié :

```shell
mount /media/usb
```

On copie les fichiers. Pour ce faire, on crée un nouveau dossier daté :

```shell
mkdir /media/hdd/cedj/usb/$( date +%Y%m%d )
rsync -av /media/usb/ /media/hdd/cedj/usb/$( date +%Y%m%d )
```

C'est le moment de ranger ces fichiers selon la structure attendue :

```text
/media/hdd/cedj/usb/
└── 20230910/
    ├── justice.sql
    └── htdocs/
        ├── BOB/
        ├── index.php
        └── [...]
```

On va dans ce dossier (disons que la mise à jour est datée `20230910`).

On commence par renommer le dump de la DB en `justice.sql` :

```shell
cd /media/hdd/cedj/usb/20230910/
mv justice (53).sql justice.sql
```

On renomme le dossier `legislation` en `htdocs` :

```shell
mv legislation htdocs
```

On s'assure que les droits sont corrects (chaque script en fait autant,
mais on préfère assurer le coup le plus tôt possible ) :

```shell
chmod u=rxX,go=rX -R /media/hdd/cedj/usb/20230910/
```

À partir de ce moment, le script `cedj-upload-to-fileserver.sh` s'occupe du reste.

```shell
cedj-upload-to-fileserver.sh
```
