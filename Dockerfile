FROM offlineinternet/php5.3-apache

ENV DB_HOST="${DB_HOST:-cedj.app.mariadb}" \
    DB_NAME="${DB_NAME:-justice}" \
    DB_USER="${DB_USER:-cedjuser}" \
    DB_PASS="${DB_PASS:-cedjpass}"

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get install --yes --no-install-recommends \
        mysql-client \
        rsync \
        supervisor \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY --chown=root:root rootfs/ /

RUN sed -i -e 's@/var/www/html@/data/htdocs@;s@/var/www@/data@' /etc/apache2/apache2.conf \
    && ln -sf /run/supervisord.log /var/log/supervisor/supervisord.log

WORKDIR /data/htdocs

EXPOSE 80
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
